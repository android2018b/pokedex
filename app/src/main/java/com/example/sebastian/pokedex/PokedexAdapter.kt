package com.example.sebastian.pokedex

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.pokedex_view_holder.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokedexAdapter( val selectionListener: PokedexSelectionListener): RecyclerView.Adapter<PokedexAdapter.PokedexViewHolder>() {

  companion object {
    val LIMIT = 20
  }

  interface PokedexSelectionListener {
    fun pokemonSelected(pokemonName: String)
    fun fetchCompleted()
  }

  var pokemonList = mutableListOf<PokedexResponse.PokemonResponse>()
  var currentOffset = 0

  init {
    loadPokemon()
  }

  fun loadPokemon() {

    val pokemonService = PokemonService.instance
    val pokemonCall = pokemonService.getPokemon(currentOffset, LIMIT)

    pokemonCall.enqueue(object : Callback<PokedexResponse> {
      override fun onFailure(call: Call<PokedexResponse>?, t: Throwable?) {
        if (call != null) {
        }
      }

      override fun onResponse(call: Call<PokedexResponse>?, response: Response<PokedexResponse>?) {

        if (response != null) {
          val body = response.body() as PokedexResponse
          pokemonList.addAll(body.results as MutableList<PokedexResponse.PokemonResponse>)
          notifyDataSetChanged()
          currentOffset += LIMIT
          selectionListener.fetchCompleted()
        }
      }
    })

  }


  override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PokedexViewHolder {
    val view = LayoutInflater.from(p0.context)
            .inflate(R.layout.pokedex_view_holder, p0, false)
    return PokedexViewHolder(view)
  }

  override fun getItemCount(): Int {
    return pokemonList.count()
  }

  override fun onBindViewHolder(p0: PokedexViewHolder, p1: Int) {

    p0.pokemonName.text = pokemonList[p1].name

    Glide.with(p0.itemView)
            .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${p1 + 1}.png")
            .into(p0.pokemonImage)

    p0.itemView.setOnClickListener {
      selectionListener.pokemonSelected(pokemonList[p1].name)
    }
  }


  class PokedexViewHolder (itemView:View) : RecyclerView.ViewHolder(itemView) {

    val pokemonName = itemView.findViewById<TextView>(R.id.pokemon_name)
    val pokemonImage = itemView.findViewById<ImageView>(R.id.pokemon_image)
  }
}
